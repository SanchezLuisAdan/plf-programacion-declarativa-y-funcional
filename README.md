# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
*[#orange] Programación
	*_ Son dos partes
		*[#95e0f5] Creativa
			*[#lightgreen] Ideas
			*[#lightgreen] Algoritmos
			*[#lightgreen] No es necesario tener una idea inicial
		*[#95e0f5] Burocrática
			*[#ffabf7] Gestión detallada de memoria
				* Sigue una secuencia de órdenes para obtener la solución deseada
			*[#ffabf7] Código maquina
	*_ Tipos
		*[#95e0f5] Declarativa
			*_ Características
				*[#lightgreen] Tareas rutinarias dejan al copilador
					* Mejora el tiempo de verificación y modificación
					* Gestiona la memoria
					* Mejor perspectiva
					* Característica de los lenguajes actuales
				*[#lightgreen] Abandonar las secuencias de órdenes que \ngestiona la memoria del ordenador
				*[#lightgreen] Usa funciones matemáticas
			*_ Objetivo
				*[#ffabf7] Librarse de las asignaciones
				*[#ffabf7] Reducir la complejidad de los programas
				*[#ffabf7] Reducir el riesgo de cometer errores
				*[#ffabf7] Programas más cortos
			*_ Ventajas
				*[#lightgreen] Tiempo de depuración más corto
				*[#lightgreen] Tiempo de desarrollo más corto
			*_ Tiene dos variantes
				*[#ffabf7] Funcional
					*_ Usa
						* Modelos Matemáticos
							*[#ff8f8f] Aplicacion de funciones
							*[#ff8f8f] Datos de entrada y de salida 
							*[#ff8f8f] Reglas de simplificación
					*_ Características
						* No hay distinción entre datos y programas
						* Hace énfasis en los aspectos creativos de la programación
						* Definir tipos de datos infinitos
						* Funciones de orden superior
							*[#ff8f8f] Toma una o más funciones como entrada
							*[#ff8f8f] Devuelve una función de salida
					*_ Ventajas
						* Mejora el razonamiento
						* Evaluación perezosa
							*[#ff8f8f] Solo se evalúa lo necesario
							*[#ff8f8f] Tipos de datos infinitos como Fibonacci
				*[#ffabf7] Lógica
					* Lógica de predicados
						*[#ff8f8f] Relación entre objetos definidos
						*[#ff8f8f] Declarativos
						*[#ff8f8f] Modelo de demostración de lógica
					* Características
						*[#ff8f8f] Se utilizan como expresiones de los programas
						*[#ff8f8f] Axiomas
						*[#ff8f8f] Reglas de inferencia
						*[#ff8f8f] Se puede definir una relación factorial
		*[#95e0f5] Imperativa
			*_ Consiste en
				*[#lightgreen] Darle instrucciones uno detrás de otro
			*_ Características
				*[#lightgreen] Se le dan muchos detalles sobre los cálculos que realiza
				*[#lightgreen] Cuello de botella
				*[#lightgreen] Usa el modelo de Von Newman
				*[#lightgreen] Modifica el estado de memoria del ordenador
			*[#lightgreen] Lenguajes
				* Fortran
				* Modulo
				* C
				* Pascal
@endmindmap
```

# Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
*[#orange] Programación Funcional
	*[#95e0f5] Paradigma de programación
		*[#ffabf7] Es la forma que se abordan los programas
		*[#ffabf7] Modelo de Von Newman
			* Los programas se almacenan en el \nmismo ordenador antes de ejecutarse
				*_ características
					*[#ff8f8f] Estado de computo
					*[#ff8f8f] Es una zona de memoria
					*[#ff8f8f] Las variables se modifican a través de la asignación
				*[#ff8f8f] La mayoría de lenguajes sigue este modelo
				
	*[#95e0f5] Calculo lambda
		*_ Es un
			*[#lightgreen] Sistema de aplicación de funciones de recursividad
		*[#lightgreen] Sistema formal
			* Definición de funciones
			* Recursión
		*_ Surge
			*[#lightgreen] ALGOL
	*[#95e0f5] Características
		*[#ffabf7] Funciones matemáticas
			* Concepto de asignación
			* Recursión
			*_ Desaparece la definición de
				* Variable
					*[#ff8f8f] Se refiere a los parámetros de entrada de las funciones
				* Bucle 
					*[#ff8f8f] Funciones recursivas
		*[#ffabf7] Estado de computo
		*[#ffabf7] El resultado de las funciones no depende de las otras
		*[#ffabf7] Transparencia referencial
			*_ Consiste 
				* Una expresión E del lenguaje puede ser sustituida por otra de igual valor V
	*[#95e0f5] Evaluación perezosa
		*[#lightgreen] Evalúa desde afuera hacia dentro
		*[#lightgreen] Memorización
			* Almacena el valor de una expresión cuyo valor ya ha sido analizado
		*[#lightgreen] Retrasas el cálculo de una expresión hasta que su valor sea necesario
		*_ Contrario a
			*[#lightgreen] Evaluación estricta
				* Evalúa de dentro hacia fuera
				* No necesita parámetros para desarrollarse
	*_ Comparación con la programación
		*[#95e0f5] Imperativa
			*[#ffabf7] Una función puede tener efectos colaterales \ny alterar el estado del computo
			*[#ffabf7] Programa
				*_ Es una
					* Colección de datos, variables modificables
					* Serie de instrucciones que operan dichos datos
			*[#ffabf7] Evalúa estricta
				* Tiempo finito para evaluar una operación
@endmindmap
```